# IDE script executor 

IDE script executor is an add-on for Blender that executes python scripts edited elsewhere. That way
it is possible to use your IDE or editor to develop python scripts. This add-on will execute your
scripts without you needing to switch to the Blender UI.  I find this greatly helps not loosing my
train of thought while scripting.

An example use case could be that you have the Blender 3D viewport on a second screen.  On your
first screen you have your editor and maybe the Blender console. This way you never have to leave
your editor. After a save, you just look to your second screen to see the result of your script.

## Activation

- Open Blender and go to Preferences > Add-ons.
- Search for the **IDE script executor** add-on and enable it.
- If so desired change the add-on preferences.

![idee preferences](idee_0.1_prefs.png "idee 0.1 preferences")

## How it works

Each second (or what you configured for the `Delay`) the path file is checked.  If the path file
exists, its content is read, which is the path to your python script. If the script is found, it
will be executed from within Blender.  The path file will be removed: your IDE or your editor is
supposed to create it again when saving a new version of your script.

## Configuring your IDE / editor

You need to configure your IDE / editor such that it writes to the path file on a save.  The
location of the path file should correspond to what you set in your add-on preferences.  The
contents of the path file is the path to the python script you want to execute in Blender.

I myself use vim for my programming. If you found out how to configure your specific IDE or editor,
please let me now by creating an issue.  I will add the way to do it to this section.

<!--
List of IDE's mentioned on https://wiki.blender.org/wiki/Developer_Intro/Environment
But mainly for C:
    Linux / CMake / QtCreator
    Linux / CMake / NetBeans
    Linux / CMake / Eclipse
    Linux / CMake / KDevelop
    OSX / CMake / Xcode
    Windows / CMake / MSVC2017/MSVC2019
    Portable / CMake / VSCode (Recommended for Linux/OSX only, *NOT* recommended for novice users on windows)
-->

#### vim ####

This is a working snippet for vimrc.  This maps the `<F7>` key to save the current file and write
its path to the path file:
    
    autocmd FileType python nmap <F7>      :w \| :!echo $(pwd)/% > /tmp/idee.path<CR>
    autocmd FileType python imap <F7> <Esc>:w \| :!echo $(pwd)/% > /tmp/idee.path<CR>

## Limitations

You need an IDE or editor that allows for a hook for writing the path file.
